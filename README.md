file_watchdog
=============

Watch one file or all the files in a directory and alert when any of them change.

Works on Ruby 1.9.3+

Options:
```
	-d, --dir STRING		File or directory to watch for changes.
	-g, --gui				Start the GUI instead of running in CLI mode. If this is specified then -d isn't necessary.
```

Example usage:

```sh
	ruby watchdog.rb -d /home/me/Desktop/full_of_files
```