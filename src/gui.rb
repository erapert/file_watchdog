
module Watchdog
	require 'listen'
	require 'tk'
	require 'tkextlib/tile'
	require_relative './color_logger'
	
	class Gui
		public
			def initialize(opts)
				@opts = opts
				@mainwindow = TkRoot.new { title 'Watchdog' }
				@log = ColorLogger::Logger.new
				buildUI @mainwindow
			end
			
			def run
				puts 'Gui running...'
				Tk.mainloop
			end
			
			def watch str
				if File.exists? str
					puts "Watching '#{str}'"
					#@dirErrorFrame.grid_forget()
					@dirErrorFrame.text = "Watching '#{str}'"
					@dirErrorFrame.foreground = '#00CC00'
					@dirErrorFrame.grid :row => 1, :column => 0
					
					# if we're already listening then we need to stop it before listening to a new dir
					@listener.stop if @listener and @listener.respond_to? 'stop'
					makeListener str
					startWatching
				else
					@dirErrorFrame.text = "Path does not exist: '#{str}'"
					@dirErrorFrame.foreground = 'red'
					@dirErrorFrame.grid :row => 1, :column => 0
				end
			end
			
			def shutdown
				if @listener and @listener.respond_to 'stop'
					@listener.stop 
				else
					@log.logr "[r]Listener either null or won't stop."
				end
				if @mainwindow
					@mainwindow.destroy
				end
			end
			
			def startWatching
				puts 'start watching'
				@listener.start if @listener and @listener.respond_to? 'start'
			end
			
			def pauseWatching
				puts 'pause watching'
				@listener.pause if @listener and @listener.respond_to? 'start'
			end
			
			def stopWatching
				puts 'stop watching'
				@listener.stop if @listener and @listener.respond_to? 'start'
			end
		
		private
			def makeListener target
				@listener = Listen.to(target) do |mod, add, rem|
					now = Time.new.strftime('%Y-%m-%d %H:%M:%S')
					if add.length > 0 or mod.length > 0 or rem.length > 0
						if @dirTree.exist? now
							#@log.logr "[r]#{now} already exists"
							item = now
						else
							#@log.logr "[m]#{now} doesn't exist, so I'm gonna make it"
							item = @dirTree.insert '', 'end', :id => now, :text => now #, :values => [ 'add' ]
						end
					end
					
					if add.length > 0
						@log.logr "[g]File Changed[/]: ([c]#{now}[/])"
						logFiles add
						insertDirTreeEvents item, now, add, 'add'
					end
					if mod.length > 0
						@log.logr "[y]Metadata Changed[/]: ([c]#{now}[/])"
						logFiles mod
						insertDirTreeEvents item, now, mod, 'mod'
					end
					if rem.length > 0
						@log.logr "[r]Removed[/]: ([c]#{now}[/])"
						logFiles rem
						insertDirTreeEvents item, now, rem, 'rem'
					end
				end
			end
			
			def logFiles files
				@log.indent 4
				files.each do |f|
					@log.logr "\"#{f}\""
				end
				@log.indent -4
			end
			
			def insertDirTreeEvents parent, time, fils, eventType
				id = "#{time}_#{eventType}"
				if @dirTree.exist? id
					node = id
				else
					node = @dirTree.insert parent, 'end', :id => id, :text => eventType, :tags => [ eventType ]
				end
				fils.each do |f|
					@dirTree.insert node, 'end', :text => f, :tags => [ eventType ]
				end
			end
			
			def buildUI root
				availableTkThemes = Tk::Tile::Style.theme_names
				TkOption.add '*tearOff', 0
				@menubar = TkMenu.new root
				@mainwindow['menu'] = @menubar
					mnu_file = TkMenu.new @menubar
					@menubar.add :cascade, :menu => mnu_file, :label => 'File'
						#mnu_file.add :command, :label => 'Clear', :command => lambda { clearList }
						#mnu_file.add :separator
						mnu_file.add :command, :label => 'Start Watching', :command => lambda { startWatching }
						mnu_file.add :command, :label => 'Pause Watching', :command => lambda { pauseWatching }
						mnu_file.add :command, :label => 'Stop Watching', :command => lambda { stopWatching }
						mnu_file.add :separator
						mnu_file.add :command, :label => 'Quit', :command => lambda { shutdown }
					mnu_themes = TkMenu.new @menubar
					@menubar.add :cascade, :menu => mnu_themes, :label => 'Themes'
						availableTkThemes.each do |t|
							mnu_themes.add :command, :label => t, :command => lambda { Tk::Tile::Style.theme_use(t) }
						end
				
				@whichDirFrame = Tk::Tile::Labelframe.new(root){ text 'Watch Path'; padding '5 5' }.pack #.grid :row => 0, :column => 0
					$watchdir = TkVariable.new @opts[:watch_dir]
					@dirErrorFrame = Tk::Tile::Label.new(@whichDirFrame) { text '' } # purposely not adding to the grid yet
					@whichDirEntry = Tk::Tile::Entry.new(@whichDirFrame) { textvariable $watchdir }.grid :row => 0, :column => 0
					@whichDirEntry.bind 'Return', lambda { watch @whichDirEntry.get() }
					@whichDirEntryBtn = Tk::Tile::Button.new(@whichDirFrame) { text 'Watch' }.grid :row => 0, :column => 1
					@whichDirEntryBtn['command'] = lambda { watch @whichDirEntry.get() }
				
				@dirFrame = Tk::Tile::Frame.new(root){ padding '5 5' }.pack :expand => true, :fill => :both
					@dirTree = Tk::Tile::Treeview.new(@dirFrame){ height 16; selectmode 'browse' } #columns 'type';
						@dirTree.column_configure '#0', :minwidth => 512
						#@dirTree.heading_configure 'type', :text => 'Type'
						#@dirTree.column_configure 'type', :minwidth => 64, :anchor => 'w'
						@dirTree.tag_configure 'add', :background => '#D1FFD1'
						@dirTree.tag_configure 'mod', :background => '#D1D1FF'
						@dirTree.tag_configure 'rem', :background => '#FFD1D1'
					@dirTree.pack :expand => true, :fill => :both
			end
	end
end