
module Watchdog
	
	require 'listen'
	require_relative './color_logger'
	
	class Cli
		public
			def initialize(opts)
				@opts = opts
				@log = ColorLogger::Logger.new
				@listener = Listen.to(@opts[:watch_dir], :debug => false) do |mod, add, rem|
					now = Time.new.strftime('%Y-%m-%d %H:%M:%S')
					if add.length > 0
						@log.logr "[g]File Changed[/]: ([c]#{now}[/])"
						logFiles add
					end
					if mod.length > 0
						@log.logr "[y]Metadata Changed[/]: ([c]#{now}[/])"
						logFiles mod
					end
					
					if rem.length > 0
						@log.logr "[r]Removed[/]: ([c]#{now}[/])"
						logFiles rem
					end
				end
			end
		
			def run
				begin
					@log
						.log('[r]Press \'q\' to quit[/]')
						.log("[r]Watching[/] '[c]#{File.expand_path(@opts[:watch_dir])}[/]'")
						.indent(4)
					@listener.start
					#sleep
					c = nil
					system 'stty raw -echo' # note, we'll have to manually move the cursor now
					while c != 'q'
						c = STDIN.getc
					end
					system 'stty -raw echo'
					@listener.stop
					@log.indent(-4).log '[r]Shutting down normally[/]'
					
				rescue
					# For some reason, hitting ctrl+c to stop prints an error message.
					# This appears to be a problem with Listen trying to log with Celluloid
					# and it's having problems logging in an interrupt trapper/handler
					# So the Listen devs broke the rule of doing one thing and doing it well...
					@log.indent(-4).log('[r]End watch.[/]')
				ensure
					@listener.stop
				end
			end
		
		private
			def logFiles files
				@log.indent 4
				files.each do |f|
					@log.logr "\"#{f}\""
				end
				@log.indent -4
			end
	end
end
