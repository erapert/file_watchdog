
if __FILE__ == $0
	require 'optparse'
	
	opts = { :gui => false }
	
	p = OptionParser.new do |o|
		o.on('-d', '--dir DIR', 'Directory to watch for changes.') { |d| opts[:watch_dir] = d }
		o.on('-g', '--gui', 'Use the GUI.') { |g| opts[:gui] = true }
	end
	
	if ARGV.length == 0
		puts p.help()
	else
		begin p.parse!
		rescue OptionParser::InvalidOption => e
			puts 'InvalidOption'
			puts p.help
		end

		if opts[:gui]
			require_relative 'src/gui'
			Watchdog::Gui.new(opts).run
		else
			if !opts[:watch_dir]
				puts p.help
				raise OptionParser::MissingArgument
			end
			
			require_relative 'src/cli'
			Watchdog::Cli.new(opts).run
		end
	end
end